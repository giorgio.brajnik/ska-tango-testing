
import threading
import time

import pytango
from datetime import datetime, timedelta
from typing import Callable, Optional, List, Dict




class TangoEventTracer:
    """
    MISSION: to represent a tango proxy client that can subscribe to change events of attributes
    of device proxies, store them as they are notified (in a thread-safe way), and support queries
    with timeouts to check if and when and who sent certain events.

    Particularly useful with a set of assertpy custom assertions.
    """
    def __init__(self):
        """
        Initialize the TangoEventTracer.
        """
        self.events: List[Dict[str, any]] = []
        self.lock = threading.Lock()

    def _event_callback(self, event: tango.EventData) -> None:
        """
        Callback function for capturing change events from Tango devices.
        """
        try:
            if event.err:
                print(f"Error in event callback: {event.errors}")
                return

            with self.lock:
                self.events.append({
                    'timestamp': datetime.now(),
                    'device': event.device,
                    'attribute': event.attr_name,
                    'current_value': event.attr_value.value,
                    'previous_value': event.attr_value.prev_value
                })
        except Exception as e:
            print(f"Exception in event callback: {e}")

    def subscribe_to_device(self, device_name: str, attribute_name: str) -> None:
        """
        Subscribe to change events for a specific attribute of a Tango device.
        """
        try:
            device_proxy = pytango.DeviceProxy(device_name)
            device_proxy.subscribe_event(attribute_name, pytango.EventType.CHANGE_EVENT,
                                         self._event_callback)
        except pytango.DevFailed as e:
            print(f"Failed to subscribe to {device_name}/{attribute_name}: {e}")
        except Exception as e:
            print(f"Unexpected exception during subscription: {e}")

    def query_events(self, predicate: Callable[[Dict[str, any]], bool],
                     timeout: Optional[int] = None) -> bool:
        """
        Query stored events based on a predicate function. Optionally, wait for a matching event until a timeout.

        Args:
            predicate: A function that takes an event as input and returns True if the event matches the desired criteria.
            timeout: The time window in seconds to wait for a matching event (optional).
            If not specified, the method returns immediately.

        Returns:
            True if a matching event is found within the timeout period, False otherwise.
        """
        end_time = datetime.now() + timedelta(seconds=timeout) if timeout is not None else None

        while True:
            try:
                with self.lock:
                    if any(predicate(event) for event in self.events):
                        return True

                if timeout is not None and datetime.now() >= end_time:
                    return False

                time.sleep(0.1)  # Sleep to prevent high CPU usage
            except Exception as e:
                # Log or handle the exception as appropriate
                print(f"Error while querying events: {e}")

    def clear_events(self) -> None:
        """
        Clear all stored events.
        """
        with self.lock:
            self.events.clear()


# what follows are examples of assertpy custom assertions that take advantage of
# the tracer class

from assertpy import add_extension, assert_that

def assert_event_occurs_within_timeout(tracer: TangoEventTracer, predicate, timeout):
    """
    Custom assertpy assertion to verify that an event matching a given predicate occurs within a specified timeout.

    Args:
        tracer (TangoEventTracer): The TangoEventTracer instance to query for events.
        predicate (Callable[[Dict[str, any]], bool]): A function that takes an event as input and returns
        True if the event matches the desired criteria.
        timeout (int): The time window in seconds to wait for a matching event.

    This assertion checks if an event that satisfies the provided predicate occurs within the given timeout period.
    If no such event is found within the timeout, it lists the existing events and raises an assertion error.

    Example Usage:
        assert_that(tracer).assert_event_occurs_within_timeout(lambda e: e['device'] == 'device1' and e['current_value'] > 50, 10)

    The above example asserts that an event from 'device1' with a current value greater than 50 should occur within 10 seconds.
    """

    def _assertion(self):
        result = tracer.query_events(predicate, timeout)
        if not result:
            event_list = "\n".join([str(event) for event in tracer.events])
            self.error(f"Expected to find an event matching the predicate within {timeout} seconds, "
                       f"but none was found. Existing events:\n{event_list}")

    return _assertion

# Add the custom extension to assertpy
add_extension(assert_event_occurs_within_timeout)



def assert_two_events_in_order_with_timeout(tracer: TangoEventTracer, first_event_predicate, second_event_predicate, timeout):
    """
    Custom assertpy assertion to verify that two specific events occur in the given order within a specified timeout.

    Args:
        tracer (TangoEventTracer): The TangoEventTracer instance to query for events.
        first_event_predicate (Callable[[Dict[str, any]], bool]): A predicate function for the first event.
        second_event_predicate (Callable[[Dict[str, any]], bool]): A predicate function for the second event.
        timeout (int): The time window in seconds to wait for the events.

    This assertion checks if two events that satisfy the provided predicates occur in order within the given timeout period.
    If the events do not occur in the expected order within the timeout, it lists the existing events and raises an assertion error.

    Example Usage:
        assert_that(tracer).assert_two_events_in_order_with_timeout(
            lambda e: e['device'] == 'device1' and e['attribute'] ==  'state' and e['current_value']=='RESOURCING',
            lambda e: e['device'] == 'device1' and e['attribute'] ==  'state' and e['current_value']=='IDLE',
            10
        )

    This checks that an event from 'device1' occurs before an event from 'device2' within 10 seconds.
    """

    def _assertion(self):
        start_time = time.time()
        first_event_occurred = False

        while time.time() - start_time < timeout:
            with tracer.lock:
                if not first_event_occurred and any(first_event_predicate(event) for event in tracer.events):
                    first_event_occurred = True

                if first_event_occurred and any(second_event_predicate(event) for event in tracer.events):
                    return

            time.sleep(0.1)  # Sleep to prevent high CPU usage

        event_list = "\n".join([str(event) for event in tracer.events])
        self.error(f"Expected to find two events in order within {timeout} seconds, but they were not found. "
                   f"Existing events:\n{event_list}")

    return _assertion

# Add the custom extension to assertpy
add_extension(assert_two_events_in_order_with_timeout)


